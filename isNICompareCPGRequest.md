# isNICompareCPGRequest

```xml
	<rule name="isNICompareCPGRequest">
		<documentation>
			Rule for identifying named insured CSE requests that need to run through the compare cpg flow.
		</documentation>
		
		<and>
			<not>
				<in param="state">
					<value>IN</value>
					<value>WA</value>
				</in>
			</not>
			<and>
				<exists param="requestForScoreReason" />
				<equals param="requestForScoreReason" value="NI" />
			</and>
			<and>
				<exists param="creditScoringEvent" />
				<equals param="creditScoringEvent" value="true" />
			</and>
			<or>
				<equals param="isStateAvailableForCPGCapping" value="true" />
				<equals param="state" value="UT" />	
			</or>
		</and>
		
	</rule>

```