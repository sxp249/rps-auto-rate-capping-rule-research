# storeRating

```xml
	<rule name="storeRating">
		<documentation>
			Determine if Rating needs to be stored in the RPS Database

			true when: rpmApplies and productStatusType specific rules
		</documentation>

		<and>
			<equals param="rpmApplies" value="true" />
			<or>
				<equals param="useSelfAssessmentBasedScores" value="true" />
				<equals param="productStatusType" value="QUOTE" />
				<and>
					<equals param="productStatusType" value="POLICY" />
					<in param="rateProcessCode">
						<value>NEWP</value>
						<value>MCHG</value>
						<value>BILL</value>
						<value>CERW</value>						
						<value>STAT</value>	
						<value>PCSL</value>					
					</in>
				</and>
			</or>
		</and>
	</rule>


```