# needsPriorRating

```xml
<rule name="needsPriorRating">
		<documentation>
			Determine if RPS needs to access a prior rating.

			true when: rpmApplies and processing code is STAT OR CERW or
			(BILL and creditScoringEvent) or (non scoring and either
			MCHG,WHATIF-midterm or RENEWAL) and ratingTimeStamp exists
			or processing code is MCHG,WHATIF_MIDTERM,WHATIF_RENEWAL and
			creditScoringEvent does not exists or is false

		</documentation>
		<and>
			<equals param="rpmApplies" value="true" />
			<or>
				<equals param="rateProcessCode" value="PCSL" />  
				<equals param="applyCPGCapping" value="true" />  
				<equals param="isNICompareCPGRequest" value="true" /> 
				<and>
					<exists param="creditAAStandardCode" />							
					<equals param="creditAAStandardCode" value="I" />
				</and>			
				<and>
					<exists param="ratingTimestamp" />
					<or>
						<equals param="bestPremiumForCredit" value="true" />						
						<equals param="useOfPreviousCreditBasedOnConsent" value="true" />						
						<in param="rateProcessCode">
							<value>BILL</value>
							<value>PCBR</value>
							<value>RCBR</value>
							<value>CBRRNCK</value>
						</in>
						<and>
							<or>
								<not>
									<exists param="creditScoringEvent" />
								</not>
								<equals param="creditScoringEvent"
									value="false" />
							</or>
							<in param="rateProcessCode">
								<value>STAT</value>
								<value>MCHG</value>
								<value>CERW</value>					
								<value>WHATIF_MIDTERM</value>
								<value>WHATIF_RENEWAL</value>
							</in>
						</and>
					</or>
				</and>
			</or>
		</and>
	</rule>

```