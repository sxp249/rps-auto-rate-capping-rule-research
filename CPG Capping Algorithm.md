```bash
Using the CAPPING procedure as reference, create a new procedure called CPG_LIMITING:

If BEST_CPG_IND is on program stack
     Call best_cpg procedure
Else
Retrieve function info from call to rate_prem_util.get_function_info
Retrieve gscp and versions from call to rate_prem_util.get_gscp_ver
Retrieve coverage from program stack
Retrieve longevity years from program stack 
Retrieve cbis from program stack
Retrieve term from program stack
Retrieve cpg term ind from program stack
Set cp score by calling get_cp score procedure using cbis and longevity
Set cpg by calling get_cpg procedure using cp score from previous call

If cpg term ind = ‘Y’
     Set cpg factor by calling get_cpg_factor procedure using term and cpg from previous call
Else
Set cpg factor by calling get_cpg_factor procedure using cpg from previous call
End if

If CAPPING_IND is on program stack

     If previous coverage cpg is not available on program stack
           Retrieve default limit cpg from program stack
     End if

Set previous CPG as PREV_coverage_CPG on program stack

     If cpg term ind = ‘Y’
           Set previous cpg factor by calling get_cpg_factor procedure using term and previous cpg
Else
           Set previous cpg factor by calling get_cpg_factor procedure using previous cpg
End if

     Set cpg limit difference by subtracting previous cpg from cpg

     If cpg limit difference <= 0 -- decrease in CPG
           Set CPG used as CURRENT
Set cpg factor as CPG_FACTOR on function stack
     Set cp score as coverage_CP_SCORE on algorithm stack
Set cpg as coverage_CPG on algorithm stack
     Set cpg factor as coverage_CPG_FACTOR on algorithm
     Set previous cpg as EVALUATED_coverage_CPG on algorithm stack
Set previous cpg factor as EVALUATED_coverage_CPG_FACTOR on algorithm stack
     Else
If cpg limit difference < minimum CPG movement
     Set CPG used as PREVIOUS
     Set previous cpg factor as CPG_FACTOR on function stack
Set previous cpg as coverage_CPG on algorithm stack
           Set previous cpg factor as coverage_CPG_FACTOR on algorithm stack
           Set cp score as EVALUATED_coverage_CP_SCORE on algorithm stack
Set cpg as EVALUATED_coverage_CPG on algorithm stack
           Set cpg factor as EVALUATED_coverage_CPG_FACTOR on algorithm stack
Else
     If cpg limit difference <= maximum CPG movement
           Set CPG used as CURRENT
Set cpg factor as CPG_FACTOR on function stack
     Set cp score as coverage_CP_SCORE on algorithm stack
Set cpg as coverage_CPG on algorithm stack
     Set cpg factor as coverage_CPG_FACTOR on algorithm
     Set previous cpg as EVALUATED_coverage_CPG on algorithm stack
Set previous cpg factor as EVALUATED_coverage_CPG_FACTOR on algorithm stack
Else
     Set CPG used as CAPPED
                     Set modified cpg by adding maximum cpg movement to previous cpg
Call get_max_cpg function using gscp, coverage and rgset version

If modified cpg > max cpg  
Set modified cpg to max cpg
End if

If cpg term ind = ‘Y’
Set modified cpg factor by calling get_cpg_factor procedure using term and modified cpg
Else
Set modified cpg factor by calling get_cpg_factor procedure using modified cpg
End if

                     Set modified cpg factor as CPG_FACTOR on function stack
Set modified cpg as coverage_CPG on algorithm stack
           Set modified cpg factor as coverage_CPG_FACTOR on algorithm stack
           Set cp score as EVALUATED_coverage_CP_SCORE on algorithm stack
Set cpg as EVALUATED_coverage_CPG on algorithm stack
Set cpg factor as EVALUATED_coverage_CPG_FACTOR on algorithm stack
End if
End if
End if
Else
     If default limit cpg is set
If cpg term ind = ‘Y’
Set cpg factor by calling get_cpg_factor procedure using term and previous cpg
Else
                Set cpg factor by calling get_cpg_factor procedure using previous cpg
End if

Set cpg factor as CPG_FACTOR on function stack
Set cpg as coverage_CPG on algorithm stack using default limit cpg
     Set cpg factor as coverage_CPG_FACTOR on algorithm
     Else
Set cpg factor as CPG_FACTOR on function stack
Set cp score as coverage_CP_SCORE on algorithm stack
Set cpg as coverage_CPG on algorithm stack
     Set cpg factor as coverage_CPG_FACTOR on algorithm
     End if
     End if
End if
```