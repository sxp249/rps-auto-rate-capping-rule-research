
# needsPriorCPG
```xml
	<rule name="needsPriorCPG">
   		<documentation>
			This rule determines if a particular flow within RPS requires previous CPG elements 
			(for rate computation). This is for doing additional data validation. This rule 
			provides more fine grained control after it has been determined that prior rating 
			is required and hence needs to be evaluated after 'needsPriorRating' rule. This was 
			added as a part of the fix for defect 1320.
   		</documentation>
		<and>
      		<equals param="rpmApplies" value="true" />
      		<exists param="ratingTimestamp" />
      		<or>
         		<not>
      				<exists param="creditScoringEvent" />
	   			</not>
         		<equals param="creditScoringEvent" value="false" />
      		</or>
      		<in param="rateProcessCode">								   
      			<value>BILL</value>
      			<value>MCHG</value>
	   			<value>CERW</value>
	   			<value>PCSL</value>
      	   		<value>STAT</value>
	   			<value>WHATIF_MIDTERM</value>
	   			<value>WHATIF_RENEWAL</value>							
	   		</in>
   		</and>
	</rule>	

```