
# 1. needsPremiums, 2 .validForNonRPM

```xml

	<rule name="needsPremiums">
		<documentation>
			Rule to determine if a need to call rating True When: Rate
			Process Code is not RCBR, PCBR we call rating
		</documentation>
		<not>
			<in param="rateProcessCode">
				<value>RCBR</value>
				<value>PCBR</value>
				<value>CBRRNCK</value>
			</in>
		</not>
	</rule>

	<rule name="validForNonRPM">
		<documentation>
			Rule to determine if a request is valid for Non-RPM
			situations True When: Rate Process Code is not RPM specific
			(RCBR, PCBR)
		</documentation>
		<not>
			<in param="rateProcessCode">
				<value>RCBR</value>
				<value>PCBR</value>
				<value>CBRRNCK</value>
			</in>
		</not>
	</rule>

```