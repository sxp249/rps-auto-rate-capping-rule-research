# bestCPGforPremium
```xml
<rule name="bestCPGforPremium">
		<documentation>
			For ID/UT determine if the request is a manual request for score 
			which includes reconsiderations (RC) incorrect report (IR), 
			exception (EC), and reorder(RO). For KS determine if the request 
			is for reconsideration.
		</documentation>
		<or>
			<and>
				<in param="state">
					<value>ID</value>
					<value>UT</value>
				</in>
				<exists param="requestForScoreReason" />
				<in param="requestForScoreReason">
		   		   	<value>RC</value>
					<value>IR</value>
					<value>EC</value>
					<value>RO</value>
				</in>
			</and>
			<and>
				<in param="state">
					<value>KS</value>
				</in>
				<exists param="requestForScoreReason" />
				<in param="requestForScoreReason">
		      		<value>RC</value>
				</in>
			</and>
			<and>
				<in param="state">
					<value>MO</value>
				</in>
				<exists param="requestForScoreReason" />
				<in param="requestForScoreReason">
		      		<value>NI</value>
				</in>
				<greaterThanEquals param="effectiveDate" type="date" value="${mo.capping.start.date}" />
			</and>
            <and>
				<in param="state">
					<value>NV</value>
				</in>
				<exists param="requestForScoreReason" />
				<in param="requestForScoreReason">
		   		   	<value>NI</value>
					<value>IR</value>
					<value>RC</value>
					<value>RO</value>
					<value>EC</value>
					<value>RI</value>					
					<value>RN</value>
				</in>
                <greaterThanEquals param="effectiveDate" type="date" value="2020-12-29" />
            </and>
		</or>
	</rule> 

```