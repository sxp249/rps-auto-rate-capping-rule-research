
# isStateAvailableForCPGCapping

```xml
<rule name="isStateAvailableForCPGCapping">
		<documentation>
			Rule for identifying whether the state is available for CPG Capping.
			MO is being shut off so that was moved from the upper list and a new date added below
		</documentation>
		
		<and>
			<equals param="rpmApplies" value="true" />
			<and>
				<or>
					<and>
						<in param="state">
							<value>IL</value>
							<value>AZ</value>
							<value>CO</value>
							<value>OH</value>
							<value>KS</value>
							<value>IA</value>
							<value>MN</value>
							<value>NE</value>
							<value>WI</value>
							<value>ND</value>
							<value>SD</value>
							<value>IN</value>
						</in>
						<greaterThanEquals param="effectiveDate" type="date" value="${capping.start.date}" />
					</and>
					<and>
						<equals param="state" value="NV" />
						<greaterThanEquals param="effectiveDate" type="date" value="${nv.capping.start.date}" />
					</and>
					<and>
						<equals param="state" value="WA" />
						<lessThan param="effectiveDate" type="date" value="${wa.capping.start.date}" />
					</and>
					<and>
						<equals param="state" value="MO" />
						<lessThan param="effectiveDate" type="date" value="${mo.capping.start.date}" />
					</and>
				</or>
			</and>				
		</and>
	</rule>


```